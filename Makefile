NAME=ldapsaisie-supann
VERSION=`git describe | tr - . | cut -c2-`
FULLNAME=$(NAME)-$(VERSION)

all:

install:

uninstall:

dist-bzip2:
	rm -rf build dist
	mkdir -p build/$(FULLNAME) sdist
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done
	cd build && tar cfj ../sdist/$(FULLNAME).tar.bz2 .
	rm -rf build

clean:
	rm -rf sdist build

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(FULLNAME))
