<?php
/*******************************************************************************
 * Copyright (C) 2014 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

$debug = false;

$supann_configs = array(
  array(
    'ldap_config' => array(
      /* adresse IP ou nom DNS du serveur LDAP */
      'host'     => '10.20.30.40',
      /* port d'écoute (devrait toujours être 389) */
      'port'     => 389,
      /* LDAPv3, ne pas changer */
      'version'  => 3,
      /* activation d'une connexion chiffrée TLS. Si true, il faut s'assurer
       * d'avoir un certificat valide sur le serveur LDAP, sinon ajouter cette
       * ligne à la fin de /etc/ldap/ldap.conf:
       *     TLS_REQCERT allow
      */
      'starttls' => false,

       /* nom de la base créée avec "newdb" */
      'basedn'   => 'dc=modifiez-moi,dc=fr',
      /* DN de l'administrateur, il faut juste modifier le suffixe dc=...  qui
       * doit être égal à la valeur du basedn précédent */
      'binddn'   => 'uid=admin,ou=people,dc=modifiez-moi,dc=fr',
      /* mot de passe de l'administrateur choisi lors du "newdb" */
      'bindpw'   => 'modifiez-moi',

      /* ne pas toucher si vous ne savez pas ce que vous faîtes ...*/
      'options'  => array(),
      'filter'   => '(objectClass=*)',
      'scope'    => 'sub'
    ),
    'globals' => array(
      /* indiquer ici le DN de l'entité parente de l'établissement,
       * par exemple
       *    supannCodeEntite=QUELQUECHOSE,ou=structures,dc=quelquechose,dc=fr
       * où QUELQUECHOSE est le code choisi lors du "newdb"
       */
      'LS_SUPANN_ETABLISSEMENT_DN' => 'supannCodeEntite=MODIFIEZMOI,ou=structures,dc=modifiez-moi,dc=fr',
      /* code UAI indiqué lors du newdb */
      'LS_SUPANN_ETABLISSEMENT_UAI' => '{UAI}9990000X',
      /* nom de domaine pour construire les eduPersonPrincipalName,
       * qui seront au format login@modifiez-moi.fr */
      'LS_SUPANN_EPPN_DOMAIN' => 'modifiez-moi.fr',
    ),
  ),
);
