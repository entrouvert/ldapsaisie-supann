<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

function before_delete_entite($object) {
  $entite = new LSsupannEntite();
  if ($entite->listChildren($object)) {
      LSerror::addErrorCode('Cette entité a des entités filles, vous ne pouvez pas la supprimer');
      return false;
  }
  return true;
}

$GLOBALS['LSobjects']['LSsupannEntite'] = array (
  'objectclass' => array(
    'supannEntite',
    'organizationalUnit'
  ),
  'rdn' => 'supannCodeEntite',
  'container_dn' => 'ou=structures',
  'display_name_format' => '%{ou}',
  'label' => 'Entités',
  'displayAttrName' => true,

  // triggers
  'before_delete' => 'before_delete_entite',

  // LSform
  'LSform' => array (
    'ajaxSubmit' => 1,
    // Layout
    'layout' => array (
      'infos' => array(
        'label' => 'Informations',
        'args' => array (
          'supannCodeEntite',
          'supannCodeEntiteParent',
          'supannTypeEntite',
          'ou',
          'description',
          'supannRefId',
        )
      ),
      'com' => array (
        'label' => 'Communication',
        'args' => array (
          'postalAddress',
          'telephoneNumber',
          'facsimileTelephoneNumber',
        )
      )
    )
  ),

	// LSrelation
	'LSrelation' => array(
		'children' => array(
			'label' => 'Children entities',
			'emptyText' => "Have any child entity.",
			'LSobject' => 'LSsupannEntite',
			'list_function' => 'listChildren',
			'getkeyvalue_function' => 'getEntiteKeyValue',
			'update_function' => 'updateChildren',
			'remove_function' => 'deleteOneChild',
			'rename_function' => 'renameOneChildren',
			'canEdit_function' => 'canEditChildren',
			'canEdit_attribute' => 'supannCodeEntiteParent',
			'rights' => array(
				'admin' => 'r'
			)
		)
	),

  'LSsearch' => array (
    'attrs' => array (
      'ou',
      'description',
      'supannCodeEntite',
      'supannRefId',
      'telephoneNumber',
      'facsimileTelephoneNumber',
      'postalAddress'
    ),
  ),

  'attrs' => array (
  
    /* ----------- start -----------*/
    'ou' => array (
      'label' => "Nom cours",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'object_type' => 'LSsupannEntite',
          'filter' => 'ou=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannCodeEntiteParent' => array (
      'label' => 'Entité parente',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannEntiteBase',
          'value_attribute' => 'supannCodeEntite'
        )
      ),
      'required' => 1,
      'multiple' => 0,
      'validation' => array (
        array (
          'msg' => "Cette entité n'existe pas.",
          'object_type' => 'LSsupannEntiteBase',
          'filter' => 'supannCodeEntite=%{val}',
          'result' => 1
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/


    /* ----------- start -----------*/
    'supannCodeEntite' => array (
      'label' => 'Identifiant de l\'entité',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'multiple' => 0,
      'check_data' => array (
        'regex' => array(
          'params' => array(
            'regex' => '/^[a-zA-Z0-9_-]+$/',
          ),
          'msg' => "L'identifiant ne peut contenir que des nombres, des lettres ou les caractères - et _."
        ),
      ),
      'validation' => array (
        array (
          'msg' => "Cet identifiant est déjà utilisé par une autre entité.",
          'object_type' => 'LSsupannEntite',
          'filter' => '(supannCodeEntite=%{val})',
          'result' => 0
        ),
        array (
          'msg' => "Cet identifiant est déjà utilisé par une organisation.",
          'object_type' => 'LSsupannOrg',
          'filter' => '(supannCodeEntite=%{val})',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'required' => 1,
      'multiple' => 0,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'facsimileTelephoneNumber' => array (
      'label' => 'Fax',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'postalAddress' => array (
      'label' => 'Adresse postale',
      'ldap_type' => 'postaladdress',
      'html_type' => 'textarea',
      'required' => 0,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'telephoneNumber' => array (
      'label' => 'Téléphone fixe',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannRefId' => array (
      'label' => 'Référence externe',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannTypeEntite' => array (
      'label' => 'Type de l\'entité',
      'ldap_type' => 'ascii',
      'html_type' => 'supannTypeEntite',
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1
      )
    ),
    /* ----------- end -----------*/


  )
);
?>
