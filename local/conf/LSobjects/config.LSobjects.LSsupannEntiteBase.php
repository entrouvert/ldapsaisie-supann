<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

$GLOBALS['LSobjects']['LSsupannEntiteBase'] = array (
  'objectclass' => array(
    'supannEntite',
  ),
  'rdn' => 'supannCodeEntite',
  'container_dn' => 'ou=structures',
  'display_name_format' => '%{ou}%{o}',
  'label' => 'Entités',

  // LSform
  'LSform' => array (
    'ajaxSubmit' => 1,
    // Layout
    'layout' => array (
    )
  ),

  'LSsearch' => array (
    'attrs' => array (
      'description',
      'supannCodeEntite',
      'supannRefId',
      'o',
      'ou',
    ),
  ),

  'attrs' => array (
    /* ----------- start -----------*/
    'supannCodeEntiteParent' => array (
      'label' => 'Entité parente',
      'ldap_type' => 'ascii',
      'html_type' => 'select_object',
      'html_options' => array(
        'selectable_object' => array(
          'object_type' => 'LSsupannEntiteBase',
          'display_name_format' => '%{description}',
          'value_attribute' => 'supannCodeEntite'
	)
      ),
      'required' => 1,
      'multiple' => 0,
    ),
    /* ----------- end -----------*/


    /* ----------- start -----------*/
    'supannCodeEntite' => array (
      'label' => 'Identifiant de l\'entité',
      'check_data' => array (
        'regex' => array(
          'params' => array(
            'regex' => '/^[a-zA-Z0-9_-]+$/',
          ),
          'msg' => "L'identifiant ne peut contenir que des nombres, des lettres ou les caractères - et _."
        ),
      ),
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'multiple' => 0,
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'multiple' => 0,
      'required' => 1,
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannRefId' => array (
      'label' => 'Référence externe',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannTypeEntite' => array (
      'label' => 'Type de l\'entité',
      'ldap_type' => 'ascii',
      'html_type' => 'supannTypeEntite',
      'multiple' => 1,
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'ou' => array (
      'label' => "Nom cours",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'object_type' => 'LSsupannEntite',
          'filter' => 'ou=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'o' => array (
      'label' => "Nom",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'object_type' => 'LSsupannOrg',
          'filter' => 'o=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
      'dependAttrs' => array (
        'eduOrgLegalName',
      )
    ),
    /* ----------- end -----------*/
  )
);
?>
