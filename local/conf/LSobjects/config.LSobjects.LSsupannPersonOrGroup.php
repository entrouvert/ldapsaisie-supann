<?php
/*******************************************************************************
 * Copyright (C) 2014 Entr'ouvert
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.

******************************************************************************/

$GLOBALS['LSobjects']['LSsupannPersonOrGroup'] = array (
	'objectclass' => array(
	),
	'filter' => '(|(&(objectclass=groupOfNames)(objectclass=supannGroupe))(&(objectclass=inetOrgPerson)(objectclass=eduPerson)(objectclass=supannPerson)))',

	'LSsearch' => array (
		'attrs' => array (
			'givenName',
			'sn',
			'cn',
			'uid',
			'mail',
			'displayName',
			'supannMailPerso',
			'cn',
			'description',
		),
		'params' => array (
			'sortBy' => 'displayName',
			'sizelimit' => 300
		),
	),
  'attrs' => array (
    /* ----------- start -----------*/
    'cn' => array (
      'label' => "Name",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
    ),
    /* ----------- end -----------*/
		/* ----------- start -----------*/
		'description' => array (
			'label' => 'Admin comment',
			'ldap_type' => 'ascii',
			'html_type' => 'textarea',
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'supannMailPerso' => array (
			'label' => 'E-mail perso',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'mail' => array (
			'label' => 'E-mail',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'sn' => array (
			'label' => 'Name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'givenName' => array (
			'label' => 'First Name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'uid' => array (
			'label' => 'Identifier',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'displayName' => array (
			'label' => 'Display name',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
		),
		/* ----------- end -----------*/
	),
);

	//"disable_creation" => true,

