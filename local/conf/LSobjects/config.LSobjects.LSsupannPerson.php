<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	02111-1307, USA.

******************************************************************************/

require('referentiels.php');

$checked = false;
function check_edupersonaffiliation($object) {
       global $checked;
       $values = $object->getValue('eduPersonAffiliation');
       
       if (!$checked && in_array('affiliate', $values) && in_array('member', $values)) {
            $checked = true;
            return false;
       }
       return true;
}
function check_edupersonprimaryaffiliation($object) {
       $primary = $object->getValue('eduPersonPrimaryAffiliation');
       $affiliations = $object->getValue('eduPersonAffiliation');
       $intersection = array_intersect($primary, $affiliations);
    
       if (! $intersection) {
            return false;
       }
       return true;
}
$GLOBALS['LSobjects']['LSsupannPerson'] = array (
	'objectclass' => array(
		'inetOrgPerson',
		'eduPerson',
		'supannPerson',
		'pslPerson',
	),
	'rdn' => 'uid',
	'container_dn' => 'ou=people',

	'LSaddons' => array (
		'supann',
	),

	//"disable_creation" => true,

	'display_name_format' => '%{displayName}',
	'label' => 'Personnes',
	'displayAttrName' => true,
	
	// LSrelation
	'LSrelation' => array(
		'groups' => array(
			'label' => 'Belongs to groups ...',
			'emptyText' => "Doesn't belong to any group.",
			'LSobject' => 'LSsupannGroup',
			'list_function' => 'listUserGroups',
			'getkeyvalue_function' => 'getMemberKeyValue',
			'update_function' => 'updateUserGroups',
			'remove_function' => 'deleteOneMember',
			'rename_function' => 'renameOneMember',
			'canEdit_function' => 'canEditGroupRelation',
			'canEdit_attribute' => 'memberUid',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			)
		)
	),
	
	// LSform
	'LSform' => array (
		'ajaxSubmit' => 1,
		// Layout
		'layout' => array (
			'Civility' => array(
				'label' => 'Civilité',
				'img' => 1, 
				'args' => array (
					'uid',
					'eduPersonPrincipalName',
					'supannCivilite',
					'sn',
					'givenName',
					'cn',
					'displayName',
					'eduPersonNickname',
					'supannAliasLogin',
					'supannRefId',
					'userPassword',
					'supannEtablissement',
					'eduPersonAffiliation',
					'eduPersonScopedAffiliation',
					'eduPersonPrimaryAffiliation',
					'description',
					'supannParrainDN',
				)
			),
			'admin' => array (
				'label' => 'Personnel',
				'args' => array (
					'supannEmpId',
					'supannActivite',
					'supannEmpCorps',
					'supannEntiteAffectation',
					'supannEntiteAffectationPrincipale',
					'supannRoleEntite',
					'supannRoleGenerique',
					'supannTypeEntiteAffectation',
				)
			),
			'school' => array (
				'label' => 'Étudiant',
				'args' => array (
					'supannEtuId',
					'supannCodeINE',
					'supannEtuInscription',
					'supannEtuAnneeInscription',
					'supannEtuCursusAnnee',
					'supannEtuDiplome',
					'supannEtuElementPedagogique',
					'supannEtuEtape',
					'supannEtuRegimeInscription',
					'supannEtuSecteurDisciplinaire',
					'supannEtuTypeDiplome',
				)
			),
			'communication' => array (
				'label' => 'Contact',
				'args' => array (
					'preferredLanguage',
					'telephoneNumber',
					'supannAutreTelephone',
					'mobile',
					'facsimileTelephoneNumber',
					'mail',
					'supannAutreMail',
					'mailForwardingAddress',
					'supannMailPerso',
					'labeledURI',
					'userCertificate',
					'postalAddress',
					'physicalDeliveryOfficeName',
					'supannListeRouge',
					'pslBadgeCSN',
				)
			),
		), // fin Layout & fin array_merge
	), // fin LSform
	
	'LSsearch' => array (
		'attrs' => array (
			'givenName',
			'sn',
			'cn',
			'uid',
			'mail',
			'displayName',
			'supannMailPerso',
			'pslBadgeCSN',
		),
		'params' => array (
			'sortBy' => 'displayName',
			'sizelimit' => 300
		),
	),

	// Attributes
	'attrs' => array (
	
		/* ----------- start -----------*/
		'uid' => array (
			'label' => 'Identifiant',
			'help_info' => 'L\'identifiant ne doit contenir que des lettres , des chiffres, des tirets, des underscores ou des points.',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'check_data' => array (
				'regex' => array(
					'msg' => "L'identifiant ne doit contenir que des lettres , des chiffres, des tirets, des underscores ou des points.",
					'params' => array(
						'regex' => '/^[-_.a-zA-Z0-9]+$/',
					),
				),
			),
			'validation' => array (
				array (
					'filter' => '(|(uid=%{val})(supannAliasLogin=%{val}))',
					'result' => 0,
					'msg' => 'Cet identifiant est déjà utilisé.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'eduPersonPrincipalName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'userPassword' => array (
			'label' => 'Password',
			'help_info' => 'Le mot de passe doit faire plus de 8 caractères.',
			'ldap_type' => 'password',
			'html_type' => 'password',
			'html_options' => array(
				'generationTool' => true,
				'autoGenerate' => false,
				'lenght' => 8,
				'chars' => array (
					array (
						'chars' => '23456789abcdefjkpqrstwxyzABCDEFJKPQRSTWXYZ',
						'nb' => 6
					)
				),
			),
			'check_data' => array(
				'password' => array(
					'msg' => 'Le mot de passe doit faire plus de 8 caractères.',
					'params' => array(
						'minLength' => 8,
					)
				)
			),
			'required' => 1,
			'rights' => array(
                                'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1,
				'lostPassword' => 1
			),
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonPrincipalName' => array (
			'label' => 'Identifiant global',
			'help_info' => 'Syntaxe identique à une adresse e-mail.',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code Xn.",
					'params' => array(
						'regex' => '/^.*@.*$/',
					),
				),
			),
			'generate_value_format' => '%{uid}@' . $GLOBALS['LS_SUPANN_EPPN_DOMAIN'],
			'validation' => array (
				array (
					'filter' => 'eduPersonPrincipalName=%{val}',
					'result' => 0,
					'msg' => 'Cet identifiant est déjà utilisé.'
				)
			),
			'view' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAliasLogin' => array (
			'label' => 'Alias de l\'identifiant',
			'help_info' => 'L\'identifiant ne doit contenir que des lettres ou des chiffres.',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 0,
			'check_data' => array (
				'alphanumeric' => array(
					'msg' => 'L\'identifiant ne doit contenir que des lettres ou des chiffres.',
				),
			),
			'validation' => array (
				array (
					'filter' => '(|(uid=%{val})(supannAliasLogin=%{val}))',
					'result' => 0,
					'msg' => 'This login is already used.'
				)
			),
			'rights' => array(
                                'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannRefId' => array (
			'label' => 'Référence externe',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'create' => 1,
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'givenName' => array (
			'label' => 'Prénom',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'cn',
				'displayName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'sn' => array (
			'label' => 'Nom',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array (
				'cn',
				'displayName'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'cn' => array (
			'label' => 'Nom complet (pour la recherche)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'generate_function' => 'generate_cn',
			'rights' => array(
				'self' => 'r',
				'admin' => 'r'
			),
			'view' => 1,
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'displayName' => array (
			'label' => 'Nom complet (pour l\'affichage)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'required' => 1,
			'generate_function' => 'generate_displayName',
			'rights' => array(
				'self' => 'r',
				'admin' => 'r'
			),
			'view' => 1,
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonNickname' => array (
			'label' => 'Diminutif',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'telephoneNumber' => array (
			'label' => 'Téléphone fixe',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAutreTelephone' => array (
			'label' => 'Téléphone fixe (autre)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'mobile' => array (
			'label' => 'Téléphone mobile',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'facsimileTelephoneNumber' => array (
			'label' => 'Fax',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'mail' => array (
			'label' => 'E-mail',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Cette adresse est invalide.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'Cette adresse est déjà utilisée.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannAutreMail' => array (
			'label' => 'E-mail (autre)',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'multiple' => 1,
			'check_data' => array (
				'email' => array(
					'msg' => "Cette adresse est invalide.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'Cette adresse est déjà utilisée.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'mailForwardingAddress' => array (
			'label' => 'E-mail de renvoi',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Cette adresse est invalide.",
				),
			),
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannMailPerso' => array (
			'label' => 'E-mail personnel',
			'ldap_type' => 'ascii',
			'html_type' => 'mail',
			'check_data' => array (
				'email' => array(
					'msg' => "Cette adresse est invalide.",
				),
			),
			'validation' => array (
				array (
					'filter' => '(&(!(uid=%{uid}))(|(mail=%{val})(supannAutreMail=%{val})(supannMailPerso=%{val})))',
					'result' => 0,
					'msg' => 'Cette adresse est déjà utilisée.'
				)
			),
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'postalAddress' => array (
			'label' => 'Adresse postale',
			'ldap_type' => 'postaladdress',
			'html_type' => 'textarea',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannCivilite' => array (
			'label' => 'Civilité',
			'ldap_type' => 'ascii',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => array(
					'M.' => 'Mr',
					'Mme' => 'Mrs.',
					'Mlle' => 'Miss.'
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuId' => array (
			'label' => 'Identifiant de scolarité',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannCodeINE' => array (
			'label' => 'Code INE',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'supannEmpId' => array (
			'label' => 'Identifiant d\'employé',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'rights' => array(
				'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEmpCorps' => array (
			'label' => "Corps d'appartenance",
			'ldap_type' => 'ascii',
			'html_type' => 'supannEmpCorps',
			'multiple' => 0,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtablissement' => array (
			'label' => 'Code établissement',
			'ldap_type' => 'ascii',
			'html_type' => 'supannEtablissement',
			'required' => 1,
			'default_value' => $GLOBALS['LS_SUPANN_ETABLISSEMENT_UAI'],
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w',
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
			),
			'dependAttrs' => array(
				'eduPersonOrgDN'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannListeRouge' => array (
			'label' => 'Sur liste rouge?',
			'ldap_type' => 'boolean',
			'ldap_options' => array(
				'true_value' => 'TRUE',
				'false_value' => 'FALSE'
			),
			'html_type' => 'boolean',
			'required' => 1,
			'default_value' => 'no',
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonAffiliation' => array (
			'label' => 'Profils eduPerson',
			'ldap_type' => 'numeric',
			'html_type' => 'select_box',
			'html_options' => array (
				'possible_values' => $liste_affiliations
			),
                        'validation' => array(
                                array(
                                        'msg' => 'Les valeurs affiliate et member sont incompatibles',
                                        'function' => 'check_edupersonaffiliation',
                                ),
                        ), 
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonPrimaryAffiliation' => array (
			'label' => 'Profil eduPerson principal',
			'ldap_type' => 'numeric',
			'html_type' => 'select_box',
			'html_options' => array (
				'possible_values' => $liste_affiliations
			),
                        'validation' => array(
                                array(
                                        'msg' => 'Le profil principal doit faire faire partie des profils',
                                        'function' => 'check_edupersonprimaryaffiliation',
                                ),
                        ), 
			'multiple' => 0,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonScopedAffiliation' => array (
			'label' => 'Profils eduPerson scopés',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "De la forme affiliation@domaine.fr.",
					'params' => array(
						'regex' => '/^(student|faculty|staff|employee|member|affiliate|alum|researcher|retired|emeritus|teacher|registered-reader)@(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

    /* ----------- start -----------*/
    'labeledURI' => array (
            'label' => "URL du site web personnel",
            'help_info' => "Veuillez entrer une URL.",
            'ldap_type' => 'ascii',
            'html_type' => 'url',
            'check_data' => array (
              'regex' => array(
                'params' => array(
									'regex' => '/^https?:\/\/.*/',
                ),
                'msg' => "Veuillez entrer une URL."
              ),
            ),
            'multiple' => 1,
            'rights' => array(
                    'admin' => 'w',
                    'self' => 'w'
            ),
            'view' => 1,
            'form' => array (
                    'modify' => 1,
                    'create' => 1
            )
    ),
    /* ----------- end -----------*/

		/* ----------- start -----------*/
		'preferredLanguage' => array (
			'label' => 'Langue préférée',
			'ldap_type' => 'ascii',
			'html_type' => 'select_list',
			'html_options' => array (
				'possible_values' => $liste_preferredLanguage
			),
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'userCertificate' => array (
			'label' => 'Certificat X.509',
			'ldap_type' => 'ascii',
			'html_type' => 'textarea',
			'rights' => array(
				'self' => 'w',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'eduPersonOrgDN' => array (
			'label' => 'DN de l\'organisation (eduPerson)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'generate_function' => 'generate_eduPersonOrgDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEntiteAffectation' => array (
			'label' => 'Entités d\'affectation',
			'ldap_type' => 'ascii',
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannEntite',
					'value_attribute' => 'supannCodeEntite'
				)
			),
			'multiple' => 1,
			'validation' => array (
				array (
					'msg' => "Cette entité n\'existe pas.",
					'object_type' => 'LSsupannEntite',
					'filter' => 'supannCodeEntite=%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array(
				'eduPersonOrgUnitDN'
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'eduPersonOrgUnitDN' => array (
			'label' => 'DN des entités d\'affectation (eduPerson)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'generate_function' => 'generate_eduPersonOrgUnitDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEntiteAffectationPrincipale' => array (
			'label' => 'Entité d\'affectation principale',
			'ldap_type' => 'ascii',
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannEntite',
					'value_attribute' => 'supannCodeEntite'
				)
			),
			'multiple' => 0,
			'validation' => array (
				array (
					'msg' => "Cette entité n'existe pas.",
					'object_type' => 'LSsupannEntite',
					'filter' => 'supannCodeEntite=%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			),
			'dependAttrs' => array(
				'eduPersonPrimaryOrgUnitDN'
			)
		),
		/* ----------- end -----------*/


		/* ----------- start -----------*/
		'eduPersonPrimaryOrgUnitDN' => array (
			'label' => 'DN de l\'entité d\'affectation principale (eduPerson)',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 0,
			'generate_function' => 'generate_eduPersonPrimaryOrgUnitDN',
			'rights' => array(
				'admin' => 'w'
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannParrainDN' => array (
			'label' => 'Parrains',
			'help_info' => 'Personnes ayant demandées l\'inscription dans l\'annuaire.',
			'ldap_type' => 'ascii',
			'ldap_options' => array (
				'LSobjectType' => 'LSsupannPerson',
				'displayFormat' => '%{displayName}'
			),
			'html_type' => 'select_object',
			'html_options' => array(
				'selectable_object' => array(
					'object_type' => 'LSsupannPerson',
					'value_attribute' => 'dn'
				)
			),
			'multiple' => 1,
			'validation' => array (
				array (
					'msg' => "Cette personne n'existe pas dans l\'annuaire.",
					'basedn' => '%{val}',
					'result' => 1
				)
			),
			'rights' => array(
				'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannActivite' => array (
			'label' => 'Activités',
			'ldap_type' => 'ascii',
			'html_type' => 'supannActivite',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/
		/* ----------- start -----------*/
		'supannRoleEntite' => array (
			'label' => 'Rôle dans les entités',
			'ldap_type' => 'ascii',
			'html_type' => 'supannRoleEntite',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannRoleGenerique' => array (
			'label' => 'Rôle dans les entités : liste de rôles',
			'ldap_type' => 'ascii',
			'html_type' => 'supannRoleGenerique',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannTypeEntiteAffectation' => array (
			'label' => 'Rôle dans les entités: liste des types d\'entité',
			'ldap_type' => 'ascii',
			'html_type' => 'supannTypeEntite',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'description' => array (
			'label' => 'Description/Commentaire',
			'ldap_type' => 'ascii',
			'html_type' => 'textarea',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuInscription' => array (
			'label' => 'Inscriptions administratives et pédagogiques',
			'ldap_type' => 'ascii',
			'html_type' => 'supannEtuInscription',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuAnneeInscription' => array (
			'label' => 'Année d\'inscription',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'integer' => array(
					'msg' => "Entrez une année."
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuCursusAnnee' => array (
			'label' => 'Cursus et année',
			'help_info' => 'Syntaxe sur deux caractères “Xn” où X est choisi parmi les valeurs L, M, D, X et n représente l\'année
			en cours dans le diplôme. Si l\'année n\'est pas connue n peut être omis.',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code Xn.",
					'params' => array(
						'regex' => '/^[LMDX][0-9]$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuDiplome' => array (
			'label' => 'Diplôme',
			'help_info' => 'Code étiqueté ex.: {SIZE}0833434',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code étiqueté.",
					'params' => array(
						'regex' => '/^{[^}]+}.+$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuElementPedagogique' => array (
			'label' => 'Élément pédagogique',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuEtape' => array (
			'label' => 'Étape',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuRegimeInscription' => array (
			'label' => 'Type d\'enseignement SISE',
			'help_info' => 'Code SISE',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code étiqueté.",
					'params' => array(
						'regex' => '/^{[^}]+}.+$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuSecteurDisciplinaire' => array (
			'label' => 'Secteur disciplénaire de diplôme ou d\'enseignement',
			'help_info' => 'Code SISE',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code étiqueté.",
					'params' => array(
						'regex' => '/^{[^}]+}.+$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'supannEtuTypeDiplome' => array (
			'label' => 'Type ou catégorie du diplôme préparé',
			'help_info' => 'Code SISE',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code étiqueté.",
					'params' => array(
						'regex' => '/^{[^}]+}.+$/',
					),
				),
			),
			'multiple' => 1,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

		/* ----------- start -----------*/
		'pslBadgeCSN' => array (
			'label' => 'Badge CSN',
			'help_info' => 'CSN du badge Mifare du membre PSL',
			'ldap_type' => 'ascii',
			'html_type' => 'text',
			'check_data' => array (
				'regex' => array(
					'msg' => "Entrez un code CSN valide (chiffres et lettres majuscules).",
					'params' => array(
						'regex' => '/^[0-9A-F]{8,14}$/',
					),
				),
			),
			'multiple' => 0,
			'rights' => array(
                                'self' => 'r',
				'admin' => 'w'
			),
			'view' => 1,
			'form' => array (
				'modify' => 1,
				'create' => 1
			)
		),
		/* ----------- end -----------*/

	) // Fin args
);
?>
