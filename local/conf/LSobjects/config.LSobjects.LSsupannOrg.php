<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

function before_delete_organization($object) {
  $org = new LSsupannEntite();
  if ($org->listChildren($object)) {
      LSerror::addErrorCode('Cette organisation a des entités filles, vous ne pouvez pas la supprimer');
      return false;
  }
  return true;
}

$GLOBALS['LSobjects']['LSsupannOrg'] = array (
  'objectclass' => array(
    'organization',
    'eduOrg',
    'supannOrg',
    'supannEntite',
  ),
  'rdn' => 'supannCodeEntite',
  'container_dn' => 'ou=structures',
  'display_name_format' => '%{o}',
  'label' => 'Organisations',
  'displayAttrName' => true,

  // triggers
  'before_delete' => 'before_delete_organization',

  // LSform
  'LSform' => array (
    'ajaxSubmit' => 1,
    // Layout
    'layout' => array (
      'infos' => array(
        'label' => 'Informations',
        'args' => array (
          'o',
          'description',
          'l',
          'supannEtablissement',
          'supannTypeEntite',
          'supannCodeEntite',
          'supannRefId',
        )
      ),
      'com' => array (
        'label' => 'Communication',
        'args' => array (
          'postalAddress',
          'telephoneNumber',
          'facsimileTelephoneNumber',
          'labeledURI',
          'eduOrgHomePageURI',
          'eduOrgSuperiorURI',
          'eduOrgWhitePagesURI',
        )
      )
    )
  ),

  // LSrelation
  'LSrelation' => array(
    'children' => array(
      'label' => 'Children entities',
      'emptyText' => "Have any child entity.",
      'LSobject' => 'LSsupannEntite',
      'list_function' => 'listChildren',
      'getkeyvalue_function' => 'getEntiteKeyValue',
      'update_function' => 'updateChildren',
      'remove_function' => 'deleteOneChild',
      'rename_function' => 'renameOneChildren',
      'canEdit_function' => 'canEditChildren',
      'canEdit_attribute' => 'supannCodeEntiteParent',
      'rights' => array(
        'admin' => 'r'
      )
    )
  ),

  'LSsearch' => array (
    'attrs' => array (
      'o',
      'l',
      'description',
      'supannCodeEntite',
      'supannRefId',
      'telephoneNumber',
      'facsimileTelephoneNumber',
      'postalAddress'
    ),
  ),

  'attrs' => array (
    /* ----------- start -----------*/
    'o' => array (
      'label' => "Nom",
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'validation' => array (
        array (
          'object_type' => 'LSsupannOrg',
          'filter' => 'o=%{val}',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      ),
      'dependAttrs' => array (
        'eduOrgLegalName',
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannEtablissement' => array (
      'label' => 'Code établissement',
      'ldap_type' => 'ascii',
      'html_type' => 'supannEtablissement',
      'required' => 1,
      'rights' => array(
        'admin' => 'w',
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1,
      ),
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannCodeEntite' => array (
      'label' => 'Identifiant de l\'entité',
      'check_data' => array (
        'regex' => array(
          'params' => array(
            'regex' => '/^[a-zA-Z0-9_-]+$/',
          ),
          'msg' => "L'identifiant ne peut contenir que des nombres, des lettres ou les caractères - et _."
        ),
      ),
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 1,
      'multiple' => 0,
      'validation' => array (
        array (
          'msg' => "Cet identifiant est déjà utilisé.",
          'object_type' => 'LSsupannEntite',
          'filter' => '(supannCodeEntite=%{val})',
          'result' => 0
        )
      ),
      'view' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'description' => array (
      'label' => 'Description',
      'ldap_type' => 'ascii',
      'html_type' => 'textarea',
      'multiple' => 0,
      'required' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'facsimileTelephoneNumber' => array (
      'label' => 'Fax',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'postalAddress' => array (
      'label' => 'Adresse postale',
      'ldap_type' => 'postaladdress',
      'html_type' => 'textarea',
      'required' => 0,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'telephoneNumber' => array (
      'label' => 'Téléphone fixe',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannRefId' => array (
      'label' => 'Référence externe',
      'ldap_type' => 'ascii',
      'html_type' => 'text',
      'required' => 0,
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'supannTypeEntite' => array (
      'label' => 'Type de l\'entité',
      'ldap_type' => 'ascii',
      'html_type' => 'supannTypeEntite',
      'multiple' => 1,
      'rights' => array(
        'admin' => 'w'
      ),
      'view' => 1,
      'form' => array (
        'modify' => 1,
        'create' => 1
      )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'eduOrgHomePageURI' => array (
        'label' => "URL du site web",
        'ldap_type' => 'ascii',
        'html_type' => 'url',
        'check_data' => array (
          'regex' => array(
            'params' => array(
							'regex' => '/^https?:\/\/.*/',
            ),
            'msg' => "Veuillez entrer une URL."
          ),
        ),
        'multiple' => 1,
        'rights' => array(
          'admin' => 'w',
        ),
        'view' => 1,
        'form' => array (
          'modify' => 1,
          'create' => 1
        )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'eduOrgLegalName' => array (
        'label' => "Nom légal",
        'ldap_type' => 'ascii',
        'html_type' => 'text',
        'multiple' => 0,
        'generate_value_format' => '%{o}',
        'rights' => array(
          'admin' => 'w',
        ),
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'eduOrgSuperiorURI' => array (
        'label' => 'URL du site web de l\'organisation parente',
        'ldap_type' => 'ascii',
        'html_type' => 'url',
        'check_data' => array (
          'regex' => array(
            'params' => array(
							'regex' => '^/https?:\/\/.*/',
            ),
            'msg' => "Veuillez entrer une URL."
          ),
        ),
        'multiple' => 1,
        'rights' => array(
          'admin' => 'w',
        ),
        'view' => 1,
        'form' => array (
          'modify' => 1,
          'create' => 1
        )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'eduOrgWhitePagesURI' => array (
        'label' => "URL du site des pages blanches",
        'ldap_type' => 'ascii',
        'html_type' => 'url',
        'check_data' => array (
          'regex' => array(
            'params' => array(
							'regex' => '/^https?:\/\/.*/',
            ),
            'msg' => "Veuillez entrer une URL."
          ),
        ),
        'multiple' => 1,
        'rights' => array(
          'admin' => 'w',
        ),
        'view' => 1,
        'form' => array (
          'modify' => 1,
          'create' => 1
        )
    ),
    /* ----------- end -----------*/

    /* ----------- start -----------*/
    'l' => array (
        'label' => "Ville",
        'ldap_type' => 'ascii',
        'html_type' => 'text',
        'multiple' => 1,
        'rights' => array(
          'admin' => 'w',
        ),
        'view' => 1,
        'form' => array (
          'modify' => 1,
          'create' => 1
        )
    ),
    /* ----------- end -----------*/
  )
);
?>
