<?php
/*******************************************************************************
 * Copyright (C) 2007 Easter-eggs
 * http://ldapsaisie.labs.libre-entreprise.org
 *
 * Author: See AUTHORS file in top-level directory.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

******************************************************************************/

/**
 * Objet Ldap supannPersonOrGroup
 *
 * @author Benjamin Dauvergne <bdauvergne@easter-eggs.com>
 */
class LSsupannPersonOrGroup extends LSldapObject {
	// ~
	/**
	 * Retourne la valeur descriptive d'affichage de l'objet
	 *
	 */
	function getDisplayName($spe='',$full=false) {
		$d = $this -> getValue('displayName');
                if (is_array($d)) {
			$d = $d[0];
		}
		if ($d && $d != ' ') {
			return $d;
		}
		$d = $this -> getValue('uid');
                if (is_array($d)) {
			$d = $d[0];
		}
		if ($d && $d != ' ') {
			return $d;
		}
		$d = $this -> getValue('cn');
                if (is_array($d)) {
			$d = $d[0];
		}
		return $d;
	}
	function getSelectArray($pattern=NULL,$topDn=NULL,$displayFormat=NULL,$approx=false,$cache=true,$filter=NULL) {
          $search = $this -> listObjects($filter, LSsession :: getTopDn(), array('pattern' => $pattern));
          $result = [];
          foreach ($search as $obj) {
             $result[$obj -> dn] = $obj -> getDisplayName();
          }
          return $result;
	}
}

?>
