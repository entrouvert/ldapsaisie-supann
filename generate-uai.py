import requests

r = requests.get('https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-principaux-etablissements-enseignement-superieur/download?format=json', verify=False)

print '<?php'
codes = [(uai, name) for uai, name in [(x['fields'].get('uai'), x['fields']['uo_lib']) for x in
                                       r.json() if x['fields'].get('uai')]]
codes.sort(key=lambda (uai, name): name)
for uai, name in codes:
    print "$GLOBALS['supannNomenclatures']['UAI']['codeEtablissement']['%s'] = \"%s\";" % (uai.encode('utf8'), name.encode('utf8').replace('"', '\\"'))
